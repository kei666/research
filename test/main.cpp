//
// Created by kei666 on 17/09/15.
//
#include <iostream>

#include <boost/program_options.hpp>

int main(int argc, char **argv) {
    namespace po = boost::program_options;
    po::options_description opt("");
    opt.add_options()
            ("help,h", "ヘルプを表示")
            ("sfm,s", "Structure From Motion");

    po::variables_map vm;

    try {
        po::store(po::parse_command_line(argc, argv, opt), vm);
    } catch (const po::error_with_option_name &e) {
        std::cout << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    po::notify(vm);
    if (vm.count("help")) {
        std::cout << opt << std::endl;
        return EXIT_SUCCESS;
    }


    return EXIT_SUCCESS;
}