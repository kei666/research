//
// Created by kei666 on 17/10/31.
//
#include <iostream>
#include <fstream>
#include <string>

#include <opencv2/opencv.hpp>
#include <opencv2/viz.hpp>
#include <opencv2/calib3d.hpp>

#include "reconstruct.h"

using namespace std;
using namespace cv;

int getdir(const string _filename, std::vector<cv::String> &files)
{
    ifstream myfile(_filename.c_str());
    if (!myfile.is_open()) {
        cout << "Unable to read file: " << _filename << endl;
        exit(0);
    } else {;
        size_t found = _filename.find_last_of("/\\");
        string line_str, path_to_file = _filename.substr(0, found);
        while ( getline(myfile, line_str) )
            files.push_back(path_to_file+string("/")+line_str);
    }
    return 1;
}



int main(int argc, char **argv)
{
    //Read input parameters
    if ( argc != 5 )
    {
        exit(0);
    }
    // Parse the image paths
    std::vector<cv::String> images_paths;
    getdir( argv[1], images_paths );
    // Build instrinsics
    float f  = atof(argv[2]),
            cx = atof(argv[3]), cy = atof(argv[4]);
    cv::Matx33d K = cv::Matx33d( f, 0, cx,
                         0, f, cy,
                         0, 0,  1);
    bool is_projective = true;
    std::vector<cv::Mat> Rs_est, ts_est, points3d_estimated;
    ok::reconstruct(images_paths, Rs_est, ts_est, K, points3d_estimated, is_projective);
    // Print output
    cout << "\n----------------------------\n" << endl;
    cout << "Reconstruction: " << endl;
    cout << "============================" << endl;
    cout << "Estimated 3D points: " << points3d_estimated.size() << endl;
    cout << "Estimated cameras: " << Rs_est.size() << endl;
    cout << "Refined intrinsics: " << endl << K << endl << endl;
    cout << "3D Visualization: " << endl;
    cout << "============================" << endl;
    cv::viz::Viz3d window("Coordinate Frame");
    window.setWindowSize(Size(500,500));
    window.setWindowPosition(Point(150,150));
    window.setBackgroundColor(); // black by default
    // Create the pointcloud
    cout << "Recovering points  ... ";
    // recover estimated points3d
    std::vector<cv::Vec3f> point_cloud_est;
    for (int i = 0; i < points3d_estimated.size(); ++i)
        point_cloud_est.push_back(cv::Vec3f(points3d_estimated[i]));
    cout << "[DONE]" << endl;
    cout << "Recovering cameras ... ";
    std::vector<cv::Affine3d> path;
    for (size_t i = 0; i < Rs_est.size(); ++i)
        path.push_back(cv::Affine3d(Rs_est[i],ts_est[i]));
    cout << "[DONE]" << endl;
    if ( point_cloud_est.size() > 0 )
    {
        cout << "Rendering points   ... ";
        cv::viz::WCloud cloud_widget(point_cloud_est, cv::viz::Color::green());
        window.showWidget("point_cloud", cloud_widget);
        cout << "[DONE]" << endl;
    }
    else
    {
        cout << "Cannot render points: Empty pointcloud" << endl;
    }
    if ( path.size() > 0 )
    {
        cout << "Rendering Cameras  ... ";
        window.showWidget("cameras_frames_and_lines", cv::viz::WTrajectory(path, cv::viz::WTrajectory::BOTH, 0.1, cv::viz::Color::green()));
        window.showWidget("cameras_frustums", cv::viz::WTrajectoryFrustums(path, K, 0.1, cv::viz::Color::yellow()));
        window.setViewerPose(path[0]);
        cout << "[DONE]" << endl;
    }
    else
    {
        cout << "Cannot render the cameras: Empty path" << endl;
    }
    cout << endl << "Press 'q' to close each windows ... " << endl;
    window.spin();

    return EXIT_SUCCESS;
}