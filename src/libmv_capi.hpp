/*M///////////////////////////////////////////////////////////////////////////////////////
 //
 //  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
 //
 //  By downloading, copying, installing or using the software you agree to this license.
 //  If you do not agree to this license, do not download, install,
 //  copy or use the software.
 //
 //
 //                           License Agreement
 //                For Open Source Computer Vision Library
 //
 // Copyright (C) 2015, OpenCV Foundation, all rights reserved.
 // Third party copyrights are property of their respective owners.
 //
 // Redistribution and use in source and binary forms, with or without modification,
 // are permitted provided that the following conditions are met:
 //
 //   * Redistribution's of source code must retain the above copyright notice,
 //     this list of conditions and the following disclaimer.
 //
 //   * Redistribution's in binary form must reproduce the above copyright notice,
 //     this list of conditions and the following disclaimer in the documentation
 //     and/or other materials provided with the distribution.
 //
 //   * The name of the copyright holders may not be used to endorse or promote products
 //     derived from this software without specific prior written permission.
 //
 // This software is provided by the copyright holders and contributors "as is" and
 // any express or implied warranties, including, but not limited to, the implied
 // warranties of merchantability and fitness for a particular purpose are disclaimed.
 // In no event shall the Intel Corporation or contributors be liable for any direct,
 // indirect, incidental, special, exemplary, or consequential damages
 // (including, but not limited to, procurement of substitute goods or services;
 // loss of use, data, or profits; or business interruption) however caused
 // and on any theory of liability, whether in contract, strict liability,
 // or tort (including negligence or otherwise) arising in any way out of
 // the use of this software, even if advised of the possibility of such damage.
 //
 //M*/

#ifndef __OPENCV_SFM_LIBMV_CAPI__
#define __OPENCV_SFM_LIBMV_CAPI__

#include <opencv2/sfm.hpp>
#include "libmv/logging/logging.h"

#include "libmv/correspondence/feature.h"
#include "libmv/correspondence/feature_matching.h"
#include "libmv/correspondence/matches.h"
#include "libmv/correspondence/nRobustViewMatching.h"

#include "libmv/simple_pipeline/bundle.h"
#include "libmv/simple_pipeline/camera_intrinsics.h"
#include "libmv/simple_pipeline/keyframe_selection.h"
#include "libmv/simple_pipeline/initialize_reconstruction.h"
#include "libmv/simple_pipeline/pipeline.h"
#include "libmv/simple_pipeline/reconstruction_scale.h"
#include "libmv/simple_pipeline/tracks.h"

////////////////////////////////////////
// Based on 'libmv_capi' (blender API)
///////////////////////////////////////

namespace ok {
    struct libmv_Reconstruction {
        EuclideanReconstruction reconstruction;
        /* Used for per-track average error calculation after reconstruction */
        Tracks tracks;
        CameraIntrinsics *intrinsics;
        double error;
        bool is_valid;
    };


//////////////////////////////////////
// Based on 'libmv_capi' (blender API)
/////////////////////////////////////

    void libmv_initLogging(const char *argv0);

    void libmv_startDebugLogging(void);

    void libmv_setLoggingVerbosity(int verbosity);


///////////////////////////////////////////////////////////////////////////////////////////////////////
// Based on the 'selectTwoKeyframesBasedOnGRICAndVariance()' function from 'libmv_capi' (blender API)
///////////////////////////////////////////////////////////////////////////////////////////////////////

/* Select the two keyframes that give a lower reprojection error
 */

    bool selectTwoKeyframesBasedOnGRICAndVariance(
            Tracks &tracks,
            Tracks &normalized_tracks,
            CameraIntrinsics &camera_intrinsics,
            int &keyframe1,
            int &keyframe2);


////////////////////////////////////////////////////////////////////////////////////////////////////
// Based on the 'libmv_cameraIntrinsicsFillFromOptions()' function from 'libmv_capi' (blender API)
////////////////////////////////////////////////////////////////////////////////////////////////////

/* Fill the camera intrinsics parameters given the camera instrinsics
 * options values.
 */

    static void libmv_cameraIntrinsicsFillFromOptions(
            const cv::sfm::libmv_CameraIntrinsicsOptions *camera_intrinsics_options,
            CameraIntrinsics *camera_intrinsics);


//////////////////////////////////////////////////////////////////////////////////////////////////////
// Based on the 'libmv_cameraIntrinsicsCreateFromOptions()' function from 'libmv_capi' (blender API)
//////////////////////////////////////////////////////////////////////////////////////////////////////

/* Create the camera intrinsics model given the camera instrinsics
 * options values.
 */

    CameraIntrinsics *libmv_cameraIntrinsicsCreateFromOptions(
            const cv::sfm::libmv_CameraIntrinsicsOptions *camera_intrinsics_options);


////////////////////////////////////////////////////////////////////////////////////////
// Based on the 'libmv_getNormalizedTracks()' function from 'libmv_capi' (blender API)
////////////////////////////////////////////////////////////////////////////////////////

/* Normalizes the tracks given the camera intrinsics parameters
 */

    void
    libmv_getNormalizedTracks(const libmv2::Tracks &tracks,
                              const libmv2::CameraIntrinsics &camera_intrinsics,
                              libmv2::Tracks *normalized_tracks);


//////////////////////////////////////////////////////////////////////////////////////////
// Based on the 'libmv_solveRefineIntrinsics()' function from 'libmv_capi' (blender API)
//////////////////////////////////////////////////////////////////////////////////////////

/* Refine the final solution using Bundle Adjustment
 */

    void libmv_solveRefineIntrinsics(
            const Tracks &tracks,
            const int refine_intrinsics,
            const int bundle_constraints,
            EuclideanReconstruction *reconstruction,
            CameraIntrinsics *intrinsics);


///////////////////////////////////////////////////////////////////////////////////
// Based on the 'finishReconstruction()' function from 'libmv_capi' (blender API)
///////////////////////////////////////////////////////////////////////////////////

/* Finish the reconstrunction and computes the final reprojection error
 */

    void finishReconstruction(
            const Tracks &tracks,
            const CameraIntrinsics &camera_intrinsics,
            libmv_Reconstruction *libmv_reconstruction);

////////////////////////////////////////////////////////////////////////////////////////
// Based on the 'libmv_solveReconstruction()' function from 'libmv_capi' (blender API)
////////////////////////////////////////////////////////////////////////////////////////

/* Perform the complete reconstruction process
 */
    libmv_Reconstruction *libmv_solveReconstruction(
            const Tracks &libmv_tracks,
            const cv::sfm::libmv_CameraIntrinsicsOptions *libmv_camera_intrinsics_options,
            cv::sfm::libmv_ReconstructionOptions *libmv_reconstruction_options);
}

#endif
