//
// Created by kei666 on 17/10/30.
//

#ifndef PROJECT_RECONSTRUCT_H
#define PROJECT_RECONSTRUCT_H

#include <vector>

#include <opencv2/opencv.hpp>

namespace ok
{
    void reconstruct(const std::vector<cv::String> images, cv::OutputArray Rs, cv::OutputArray Ts,
                    cv::InputOutputArray K, cv::OutputArray points3d, bool is_projective) ;
}

#endif //PROJECT_RECONSTRUCT_H
