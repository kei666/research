//
// Created by kei666 on 17/10/30.
//

#ifndef PROJECT_SFM_H
#define PROJECT_SFM_H

#include <opencv2/opencv.hpp>
#include <opencv2/sfm.hpp>


#include "libmv_capi.hpp"

namespace ok{
    class SFM : public cv::sfm::BaseSFM
    {
    public:
        /** @brief Calls the pipeline in order to perform Eclidean reconstruction.
          @param points2d Input vector of vectors of 2d points (the inner vector is per image).
          @note
            - Tracks must be as precise as possible. It does not handle outliers and is very sensible to them.
        */
        CV_WRAP
        virtual void run(cv::InputArrayOfArrays points2d) = 0;

        /** @brief Calls the pipeline in order to perform Eclidean reconstruction.
          @param points2d Input vector of vectors of 2d points (the inner vector is per image).
          @param K Input/Output camera matrix \f$K = \vecthreethree{f_x}{0}{c_x}{0}{f_y}{c_y}{0}{0}{1}\f$. Input parameters used as initial guess.
          @param Rs Output vector of 3x3 rotations of the camera.
          @param Ts Output vector of 3x1 translations of the camera.
          @param points3d Output array with estimated 3d points.
          @note
            - Tracks must be as precise as possible. It does not handle outliers and is very sensible to them.
        */
        CV_WRAP
        virtual void run(cv::InputArrayOfArrays points2d, cv::InputOutputArray K, cv::OutputArray Rs,
                         cv::OutputArray Ts, cv::OutputArray points3d) = 0;

        /** @brief Calls the pipeline in order to perform Eclidean reconstruction.
          @param images a vector of string with the images paths.
          @note
            - The images must be ordered as they were an image sequence. Additionally, each frame should be as close as posible to the previous and posterior.
            - For now DAISY features are used in order to compute the 2d points tracks and it only works for 3-4 images.
        */
        virtual void run(const std::vector<cv::String> &images) = 0;

        /** @brief Calls the pipeline in order to perform Eclidean reconstruction.
          @param images a vector of string with the images paths.
          @param K Input/Output camera matrix \f$K = \vecthreethree{f_x}{0}{c_x}{0}{f_y}{c_y}{0}{0}{1}\f$. Input parameters used as initial guess.
          @param Rs Output vector of 3x3 rotations of the camera.
          @param Ts Output vector of 3x1 translations of the camera.
          @param points3d Output array with estimated 3d points.
          @note
            - The images must be ordered as they were an image sequence. Additionally, each frame should be as close as posible to the previous and posterior.
            - For now DAISY features are used in order to compute the 2d points tracks and it only works for 3-4 images.
        */
        virtual void run(const std::vector<cv::String> &images, cv::InputOutputArray K, cv::OutputArray Rs,
                         cv::OutputArray Ts, cv::OutputArray points3d) = 0;

        /** @brief Returns the computed reprojection error.
        */
        CV_WRAP
        virtual double getError() const = 0;

        /** @brief Returns the estimated 3d points.
          @param points3d Output array with estimated 3d points.
        */
        CV_WRAP
        virtual void getPoints(cv::OutputArray points3d) = 0;

        /** @brief Returns the refined camera calibration matrix.
        */
        CV_WRAP
        virtual cv::Mat getIntrinsics() const = 0;

        /** @brief Returns the estimated camera extrinsic parameters.
          @param Rs Output vector of 3x3 rotations of the camera.
          @param Ts Output vector of 3x1 translations of the camera.
        */
        CV_WRAP
        virtual void getCameras(cv::OutputArray Rs, cv::OutputArray Ts) = 0;

        /** @brief Setter method for reconstruction options.
          @param libmv_reconstruction_options struct with reconstruction options such as initial keyframes,
            automatic keyframe selection, parameters to refine and the verbosity level.
        */
        CV_WRAP
        virtual void
        setReconstructionOptions(const cv::sfm::libmv_ReconstructionOptions &libmv_reconstruction_options) = 0;

        /** @brief Setter method for camera intrinsic options.
          @param libmv_camera_intrinsics_options struct with camera intrinsic options such as camera model and
            the internal camera parameters.
        */
        CV_WRAP
        virtual void
        setCameraIntrinsicOptions(const cv::sfm::libmv_CameraIntrinsicsOptions &libmv_camera_intrinsics_options) = 0;

        /** @brief Creates an instance of the SFMLibmvEuclideanReconstruction class. Initializes Libmv. */
        static cv::Ptr<SFM>
        create(const cv::sfm::libmv_CameraIntrinsicsOptions &camera_instrinsic_options=cv::sfm::libmv_CameraIntrinsicsOptions(),
               const cv::sfm::libmv_ReconstructionOptions &reconstruction_options=cv::sfm::libmv_ReconstructionOptions());
    };
}


#endif //PROJECT_SFM_H
