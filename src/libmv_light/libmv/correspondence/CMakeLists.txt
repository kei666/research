# define the source files
SET(CORRESPONDENCE_SRC feature_matching.cc
                       matches.cc
                       nRobustViewMatching.cc)

#define the header files (make the headers appear in IDEs.)
FILE(GLOB CORRESPONDENCE_HDRS *.h)

include_directories(../..)

ADD_LIBRARY(ccorrespondence STATIC ${CORRESPONDENCE_SRC} ${CORRESPONDENCE_HDRS})

TARGET_LINK_LIBRARIES(ccorrespondence mmultiview)