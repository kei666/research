//
// Created by kei666 on 17/10/30.
//

#include "reconstruct.h"
#include "sfm.h"

using namespace cv;
using namespace libmv2;
using namespace cv::sfm;

namespace {

}

namespace ok {
    template<class T>
    void
    ori_reconstruct_(const T &input, OutputArray Rs, OutputArray Ts, InputOutputArray K, OutputArray points3d, const bool refinement=true)
    {
        // Initial reconstruction
        const int keyframe1 = 1, keyframe2 = 2;
        const int select_keyframes = 1; // enable automatic keyframes selection
        const int verbosity_level = -1; // mute libmv logs

        // Refinement parameters
        const int refine_intrinsics = ( !refinement ) ? 0 :
                                      SFM_REFINE_FOCAL_LENGTH | SFM_REFINE_PRINCIPAL_POINT | SFM_REFINE_RADIAL_DISTORTION_K1 | SFM_REFINE_RADIAL_DISTORTION_K2;

        // Camera data
        Matx33d Ka = K.getMat();
        const double focal_length = Ka(0,0);
        const double principal_x = Ka(0,2), principal_y = Ka(1,2), k1 = 0, k2 = 0, k3 = 0;

        // Set reconstruction options
        libmv_ReconstructionOptions reconstruction_options(keyframe1, keyframe2, refine_intrinsics, select_keyframes, verbosity_level);

        libmv_CameraIntrinsicsOptions camera_instrinsic_options =
                libmv_CameraIntrinsicsOptions(SFM_DISTORTION_MODEL_POLYNOMIAL,
                                              focal_length, principal_x, principal_y,
                                              k1, k2, k3);

        //-- Instantiate reconstruction pipeline
        Ptr<BaseSFM> reconstruction =
                SFM::create(camera_instrinsic_options, reconstruction_options);

        //-- Run reconstruction pipeline
        reconstruction->run(input, K, Rs, Ts, points3d);

    }

    void reconstruct(const std::vector<cv::String> images, OutputArray Rs, OutputArray Ts,
                    InputOutputArray K, OutputArray points3d, bool is_projective) {
        const int nviews = static_cast<int>(images.size());
        CV_Assert(nviews >= 2);

        // Projective reconstruction

        if (is_projective) {
            ori_reconstruct_(images, Rs, Ts, K, points3d);
        }


            // Affine reconstruction

        else {
            // TODO: implement me
            CV_Error(Error::StsNotImplemented, "Affine reconstruction not yet implemented");
        }
    }
}
